var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var votosRouter = require('./routes/votos');
var votarRouter = require('./routes/votar');
var datosRouter = require('./routes/datos');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());


app.use(function errorHandler(err, req, res, next) {
  res.status(500).send('error');
})

app.use('/', indexRouter);
app.use('/api/users', usersRouter);
app.use('/api/votos', votosRouter);
app.use('/api/votar', votarRouter);
app.use('/api/datos', datosRouter);

module.exports = app;
