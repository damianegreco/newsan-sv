var express = require('express');
var router = express.Router();
const axios = require('axios');
const con = require('../src/conexion')
const assert = require('assert');

async function checkStatus(){
  return new Promise(function(resolve, reject) {
    if(con.state === 'disconnected'){
      con.connect(function(err) {
        if (err) {
          console.log(err);
          resolve(false)
        } else {
          resolve(true)
        }
      });
    } else {
      resolve(true)
    }
  })
}

router.post('/', async(req, res, next) => {
  let datos = req.body;
  axios.get('http://worldtimeapi.org/api/timezone/America/Argentina/Ushuaia')
  .then((response) => {
    const actual = new Date(response.data.datetime);
    let dia = actual.getDate()
    if (dia < 10 ) dia = "0" + dia
    let mes = actual.getMonth() + 1
    if (mes < 10 ) mes = "0" + mes
    const anio = actual.getFullYear()
    const fecha = anio + "-" + mes + "-" + dia
    datos.fecha = fecha;
    datos.hora = actual.toLocaleTimeString();
    const collection = db.collection('buffet');
    collection.insert(datos, function(err, result) {
      if (err){
        console.log("error")
        res.status(500).send('error')
      } else {
        console.log("Voto registrado. Puesto: '" + datos.puesto + "'. Fecha y hora: " + datos.fecha + " " + datos.hora);
        res.send('guardado')
      }
      //callback(result);
    });
  })
  .catch(error => {
    console.log(error);
  });
});

module.exports = router;
