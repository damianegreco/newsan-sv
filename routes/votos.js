var express = require('express');
var router = express.Router();
var con = require('../src/conexion')


async function checkStatus(){
  // return new Promise(function(resolve, reject) {
  //   if(con.state === 'disconnected'){
  //     con.connect(function(err) {
  //       if (err) {
  //         console.log(err);
  //         resolve(false)
  //       } else {
  //         resolve(true)
  //       }
  //     });
  //   } else {
  //     resolve(true)
  //   }
  // })
  return true
}

router.get('/', async(req, res, next) => {
  if (await checkStatus()){
    const datos = req.body;
    con.query("SELECT * FROM buffet WHERE DATE(fecha) BETWEEN '" + datos.desde + "' AND '" + datos.hasta + "'", function (err, result, fields) {
      if (err) {
        console.log(err);
        res.status(500).send('error BD')
      } else {
        res.send(result);
      }
    });
  } else {
    console.log('base de datos desconectada')
    res.status(500).send('DB desconectada')
  }
});

router.post('/filtro', async(req, res, next) => {
  if (await checkStatus()){
    const datos = req.body;
    let consulta = {}

    if ( datos.puesto != null ){
      Object.assign(consulta, {puesto:datos.puesto});
    }
    if ( datos.horaDesde != null && datos.horaHasta != null){
      // if (consulta != '') {
      //   consulta += ' AND '
      // } else {
      //   consulta = ' WHERE '
      // }
      // consulta += "TIME(fecha) BETWEEN '" + datos.horaDesde + "' AND '" + datos.horaHasta + "'";
    }

    db.collection('buffet').find(consulta).toArray(function(err, result) {
      if (err) {
        console.log(err);
        res.status(500).send('error BD')
      } else {
        let resultados = []
        if ( datos.desde != null && datos.hasta != null ){
          const from = new Date(datos.desde)
          let to = new Date(datos.hasta)
          to.setDate(to.getDate()+1)
          result.forEach((item) => {
            const check = new Date(item['fecha'])
            if((check.getTime() <= to.getTime() && check.getTime() >= from.getTime())) {
              resultados.push(item)
            }
          })
        } else {
          resultados = result
        }
        if (resultados.length > 0) {
          let puestos = []
          resultados.forEach((voto) => {
            let existe = puestos.find((elem) => {
              return elem.puesto === voto.puesto;
            })
            if (!existe) puestos.push({puesto:voto.puesto})
          })
           let busqueda = puestos.map((item) => {
             item.votos = resultados.filter((voto) => voto.puesto === item.puesto)
             return item
           })
           busqueda = busqueda.map((puesto) => {
             puesto.data = [["00:00 - 06:00"],["06:00 - 12:00"],["12:00 - 18:00"],["18:00 - 00:00"]]
             return puesto
           })
           busqueda = busqueda.map((puesto) => {
             puesto.votos.forEach((voto) => {
               if (voto.hora > '00:00' &&  voto.hora < '06:00'){
                 puesto.data[0].push(voto.calificacion)
               } else if (voto.hora > '06:00' &&  voto.hora < '12:00'){
                 puesto.data[1].push(voto.calificacion)
               } else if (voto.hora > '12:00' &&  voto.hora < '18:00'){

                 puesto.data[2].push(voto.calificacion)
               } else {
                 puesto.data[3].push(voto.calificacion)
               }
             })
             delete puesto.votos
             return puesto
           })
           busqueda = busqueda.map((puesto) => {
            puesto.data = puesto.data.map((dato) => {
               let votos = ['',0,0,0,0,0]
               dato = dato.map((calif) => {
                 if (calif === 5) {
                   votos[1]+=1
                 } else if (calif === 4) {
                   votos[2]+=1
                 } else if (calif === 3) {
                   votos[3]+=1
                 } else if (calif === 2) {
                   votos[4]+=1
                 } else if (calif === 1) {
                   votos[5]+=1
                 } else {
                   votos[0] = calif
                 }
                 return calif
               })
               dato = votos
               return dato
             })
             puesto.data.splice(0,0,['Rango horario', '5', '4', '3', '2', '1'])
             return puesto
           })
           res.send(busqueda);
         } else {
           res.status(200).send([]);
         }
       }
    });
    } else {
      console.log('base de datos desconectada')
      res.status(500).send('DB desconectada')
    }
});

module.exports = router;
