var express = require('express');
var router = express.Router();
const con = require('../src/conexion')

async function checkStatus(){
  // return new Promise(function(resolve, reject) {
  //   if(con.state === 'disconnected'){
  //     con.connect(function(err) {
  //       if (err) {
  //         console.log(err);
  //         resolve(false)
  //       } else {
  //         resolve(true)
  //       }
  //     });
  //   } else {
  //     resolve(true)
  //   }
  // })
  return true
}

async function getFechas(){

  return new Promise(function(resolve, reject) {
    // Insert some documents
    db.collection('buffet').find({},{projection: {fecha:1,_id:0}}).toArray(function(err, result) {
      if (err) reject (err);
      let fechas = result.map((item) => {
        return (new Date(item.fecha));
      })
      let max = new Date(Math.max.apply(null,fechas));
      let min = new Date(Math.min.apply(null,fechas));
      fechas = [min,max]
      fechas = fechas.map((fecha) => {
        let dia = fecha.getDate() + 1
        if (dia < 10 ) dia = "0" + dia
        let mes = fecha.getMonth() + 1
        if (mes < 10 ) mes = "0" + mes
        const anio = fecha.getFullYear()
        const fechaString = anio + "-" + mes + "-" + dia
        return (fechaString)
      })
      resolve ({fechas:fechas});
    });
  })
}

async function getPuestos(){
  return new Promise(function(resolve, reject) {
    db.collection('buffet').distinct('puesto', function(err, result) {
      if (err) reject (err);
      resolve ({puestos:result});
    });
  })
}

/* GET users listing. */
router.get('/', async(req, res, next) =>{
  if (await checkStatus()){
    let datos = {};
    try {
      Object.assign(datos, await getFechas());
      Object.assign(datos, await getPuestos());
      res.send(datos);
    } catch(e){
      console.log(e);
      res.status(500).send('error');
    }
  } else {
    console.log('base de datos desconectada')
    res.status(500).send('DB desconectada')
  }
});

router.get('/fechas', async(req, res, next) =>{
  if (await checkStatus()){
    getFechas()
    .then(function(fechas){
      res.send(fechas);
    })
  } else {
    console.log('base de datos desconectada')
    res.status(500).send('DB desconectada')
  }
});

router.get('/puestos', async(req, res, next) =>{
  if (await checkStatus()){
    getPuestos()
    .then(function(puestos){
      res.send(puestos);
    })
  } else {
    console.log('base de datos desconectada')
    res.status(500).send('DB desconectada')
  }
})

module.exports = router;
