var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
const saltRounds = 10;
var con = require('../src/conexion')

async function checkStatus(){
  return new Promise(function(resolve, reject) {
    if(con.state === 'disconnected'){
      con.connect(function(err) {
        if (err) {
          console.log(err);
          resolve(false)
        } else {
          resolve(true)
        }
      });
    } else {
      resolve(true)
    }
  })
}


/* GET users listing. */
router.post('/login', async(req, res, next) => {
  datos = req.body;
  if (await checkStatus()){
    if (datos.user != null && datos.pass != null){
      con.query("SELECT id, pass FROM users WHERE user = ?", [datos.user], function (err, result, fields) {
        if (err) {
          console.log(err);
          res.status(500).send('error BD')
        } else {
        //   if (result.length >0){
        //     if (result[0].pass == datos.pass){
        //       require('crypto').randomBytes(48, function(err, buffer) {
        //         var token = buffer.toString('hex');
        //         con.query("UPDATE users SET token = ? WHERE id = ?", [token,result[0].id], function (err, result, fields) {
        //           if (err) throw err;
        //           res.status(202).send({'token':token})
        //         });
        //       });
        //     } else {
        //       res.send('usuario y/o contraseña incorrecta')
        //     }
        //   });
        // } else {
        //   res.send('usuario y/o contraseña incorrecta')
        // }
          res.status(200).send('trabajando...')
        }
      })
    } else {
      res.status(500).send(datos);
    }
  } else {
    console.log('base de datos desconectada')
    res.status(500).send('DB desconectada')
  }
});

module.exports = router;
